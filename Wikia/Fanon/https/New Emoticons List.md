**NOTE: THIS LIST DOES *NOT* INCLUDE THE CHANGES SUGGESTED [HERE](https://gitlab.com/Wychmire/Examples/blob/master/Wikia/Fanon/https/Non-Fanon_CC-emoticons.md)**


Copy the following text block and paste onto https://wingsoffirefanon.wikia.com/wiki/MediaWiki:Emoticons?action=edit

```
==Wings Of Fire Emotes==
=== Tribe Pixels ===
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/1/1d/(icewing).png
** (icewing)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/0/01/(mudwing).png
** (mudwing)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/1/19/(nightwing).png
** (nightwing)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/c/c0/(rainwing).gif
** (rainwing)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/4/49/(sandwing).png
** (sandwing)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/9/9a/(seawing).png
** (seawing)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/e/e5/(skywing).png
** (skywing)

*https://vignette.wikia.nocookie.net/wingsoffirefanon-testing/images/1/13/Nightwingcursor.png
** (nightwing-new)
*https://vignette.wikia.nocookie.net/wingsoffirefanon-testing/images/3/36/Rainwingcursor.png
** (rainwing-new)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/1/15/Sandwing-new.png
** (sandwing-new)

*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/5/59/(aviwing)-treedragon.png
** (aviwing)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/2/23/(deathwing)-treedragon.png
** (deathwing)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/e/eb/(driftwing)-treedragon.png
** (driftwing)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/b/bd/(flamewing)-treedragon.png
** (flamewing)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/6/68/(leafwing)-treedragon.png
** (leafwing)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/1/19/(swiftwing)-treedragon.png
** (swiftwing)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/d/dd/TempestWing_pixel.png
** (tempestwing)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/6/6f/(trickwing)-treedragon.png
** (trickwing)

*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/a/a7/Kip.png
** (victor)
** (vic)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/b/be/Formalkip.png
** (victor2)
** (vic2)

*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/9/9b/Golddragon-animated.gif
** (gold)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/6/6c/Silverdragon-animated.gif
** (silver)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/e/ed/Bronzedragon-animated.gif
** (bronze)



=== User Emotes ===
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/9/9b/(salamence).png
** (heliosanctus)
** (salamence)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/a/a2/Tiny_maple.png
** (icebutterfly116)
** (maple)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/e/e6/(mysterygirl000).png
** (mysterygirl000)
** (ember)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/9/95/Blacklightningpx.png
** (nightstrike the dragon)
** (stormbreak)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/4/4b/Outclawemote.png
** (outclaw)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/a/a7/Proud-dust.png
** (proud-dust)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/b/b4/(wings-of-bloodfire).png
** (wings-of-bloodfire)
** (wings)

=== Other ===
<!-- Moon Pixel by [[User:Typhoonflame]] -->
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/4/4b/Moon_pixel.png
** (moon)
** (moonwatcher)

<!-- Scroll by [[User:Danni Domini]] -->
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/5/54/Scrollemote.png
** (books)
** (scroll)

<!-- Dragon Expressions by [[User:RimeTheIcewing]] -->
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/f/fa/Happy_fin.png
** (happy)
** :-)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/3/3b/Content_fin.png
** (content)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/2/24/Unamused_fin.png
** (unamused)
** :-|

<!-- Other -->
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/8/8c/C8cc12daa9e7d9a46ec5a1312a4d17ec-d6xt8d4.gif
** (glory)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/b/b8/Morrowseer_by_Tundra.png
** (morrowseer)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/f/f2/E49a6dafed43f18c7b6681c49f0a095f-d7mdbb0.gif
** (sunny)

<!-- Facetalon Pixels by [[User:Luster the rainwing]] -->
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/1/13/Output_lTa8Uk.gif
** (facetalon)
** (ft)
** (facetalon1)
** (ft1)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/a/a4/Output_smnUgQ.gif
** (facetalon2)
** (ft2)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/8/84/Output_o6oIyv.gif
** (facetalon3)
** (ft3)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/6/6f/Output_IqzZAa.gif
** (facetalon4)
** (ft4)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/1/1a/Output_Xlcse4.gif
** (facetalon5)
** (ft5)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/3/31/Output_RkpNb6.gif
** (facetalon6)
** (ft6)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/8/84/Output_QmcRKJ.gif
** (facetalon7)
** (ft7)

<!-- Thumb Emotes by [[User:Wolfkeep]] -->
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/4/4c/Icethumb.png
** (yes1)
** (y1)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/2/23/Mudthumb.png
** (yes2)
** (y2)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/4/47/Nightthumb.png
** (yes3)
** (y3)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/7/73/Rainthumb.png
** (yes4)
** (y4)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/c/c1/Sandthumb.png
** (yes5)
** (y5)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/4/46/Seathumb.png
** (yes6)
** (y6)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/2/2c/Skythumb.png
** (yes)
** (y)
** (yes7)
** (y7)

<!-- Blushing Dragons by [[User:CopperWingz]] -->
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/c/c3/Icewing_blush.jpg
** (blush1)
** (blush)
** :-]
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/4/42/Mudwing_blush.jpg
** (blush2)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/b/b7/Nightwing_blush.jpg
** (blush3)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/5/5d/Rainwing_blush.jpg
** (blush4)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/f/f1/Sandwing_blush.jpg
** (blush5)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/d/d6/Seawing_blush.jpg
** (blush6)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/9/92/Skywing_blush.jpg
** (blush7)


=== Tokens ===
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/3/3e/IceWing-token.png
** (icetoken)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/1/1e/MudWing-token.png
** (mudtoken)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/5/53/NightWing-token.png
** (nighttoken)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/3/32/RainWing-token.png
** (raintoken)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/d/d6/SandWing-token.png
** (sandtoken)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/a/ac/SeaWing-token.png
** (seatoken)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/5/5c/SkyWing-token.png
** (skytoken)


=== Books ===
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/e/ec/Book1-emote.png
** (book1)
** (TDP)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/2/2e/Book2-emote.png
** (book2)
** (TLH)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/1/16/Book3-emote.png
** (book3)
** (THK)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/c/ca/Book4-emote.png
** (book4)
** (TDS)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/9/9e/Book5-emote.png
** (book5)
** (TBN)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/6/6a/Book6-emote.png
** (book6)
** (MR)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/6/6f/Book7-emote.png
** (book7)
** (WT)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/9/96/Book8-emote.png
** (book8)
** (EP)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/2/2b/Book9-emote.png
** (book9)
** (TOP)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/4/42/Book10-emote.png
** (book10)
** (DOD)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/3/30/Legends1.png
** (legends1)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/e/eb/Winglets1-emote.png
** (winglets1)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/4/41/Winglets2-emote.png
** (winglets2)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/6/6a/Winglets3-emote.png
** (winglets3)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/c/cd/Winglets4-emote.png
** (winglets4)



==Food Emotes==
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/b/b9/YUM_CAKE.png
** (cake)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/7/7d/HotHeadBurritosChocolateChipCookie.png
** (cookie)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/e/ef/Oreoo.png
** (oreo)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/4/43/Piepie.png
** (pie)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/8/8e/PotatoEmote.png
** (potato)



==Pokemon Emotes==
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/d/da/Tumblr_inline_mk6cfxBf9D1qz4rgp.gif
** (charmander)
** (chase)
*https://vignette.wikia.nocookie.net/whatever-you-want/images/5/57/Tumblr_lew8wiJ4l21qek1e3o1_400.gif
** (eevee)
** (cutie)
*https://vignette.wikia.nocookie.net/central/images/9/93/Emoticon_jigglypuff.png
** (jigglypuff)
** (puff)
*https://vignette.wikia.nocookie.net/central/images/a/ae/Mudkip.png
** (mudkip)
** (ohai)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/a/af/Tumblr_mjgv8kEuMg1s87n79o1_400.gif
** (pikachu)
** (run)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/3/35/Tumblr_mgqz61ozAI1rgpyeqo1_250.gif
** (squirtle)

*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/7/77/Team-instinct.png
** (instinct)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/7/7e/Team-mystic.png
** (mystic)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/0/0d/Team-valor.png
** (valor)



==Weapons==
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/7/7b/Nuke_3_jpg.jpg
** (nuke)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/9/98/Tumblr_static_aljbefiav0w80c40g0kogwggc.gif
** (knife)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/7/74/Sparkly.gif
** (gun)
** (sparkle)



==Other Emotes==
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/b/ba/SLIMEY.png
** (asdf)
** (blob)
** (slime)
*https://vignette.wikia.nocookie.net/central/images/6/6d/Emoticon_Hammer.png
** (ban)
** (hammer)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/a/a6/Boi.jpg
** (boi)
*https://vignette.wikia.nocookie.net/central/images/d/d7/Emoticon-bug.gif
** (bug)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/7/76/Chair.png
** (chair)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/a/a6/THE_CHICKEN.png
** (chicken)
*https://vignette.wikia.nocookie.net/legomessageboards/images/d/d5/Creeper_face.png
** (creeper)
*https://vignette.wikia.nocookie.net/central/images/c/cd/Emoticon-dance.gif
** (dance)
*https://vignette.wikia.nocookie.net/central/images/2/22/Emoticon-walrus.png
** (fatespeaker)
** (walrus)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/b/b5/10D8j2EpNCXDA4.gif
** (fite)
** (get out)
*https://vignette.wikia.nocookie.net/mspaintadventures/images/d/d6/Fruits.gif
** (fruitparty)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/f/f6/My_Fandom_Needs_Me.gif
** g2g
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/a/a0/Noneedtobeupset.gif
** (jimmies)
** (noneedtobeupset)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/e/ee/Locks-knob-3400.png
** (knob)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/a/a9/SUzxG.gif
** (kool aid)
** (oh yeah)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/5/5b/Meee.png
** (lamprey)
** (*O*)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/d/d1/The_BLOB.png
** (lightning)
*https://vignette.wikia.nocookie.net/central/images/9/95/Emoticon-ninja.gif
** (ninja2)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/e/e6/Giphy_(8).gif
** (sbow)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/7/7e/Unicorn.png
** (unicorn)
** (unikat)



==General Emotes==
*https://vignette.wikia.nocookie.net/central/images/d/d9/Wave_emoticon.gif
** o/
** (wave)
*https://vignette.wikia.nocookie.net/central/images/9/99/Wave_emoticon2.gif
** \o
** (backwave)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/9/96/7f0bccb662b205850c0f5da998179bd1.png
** (clap)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/2/2e/Fbe9c2dfe740a8313854d7b0318b8d0b.png
** (eyes)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/e/e7/(ok).png
** (ok)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/0/02/(peace).png
** (p)
*https://vignette.wikia.nocookie.net/central/images/2/22/Emoticon-star.gif
** (star)
** (*)
*https://vignette.wikia.nocookie.net/bakugan/images/e/e1/Emoticon-Sun.gif
** (sun)
*https://vignette.wikia.nocookie.net/wingsoffirefanon/images/0/05/....gif
** (...)



==Default Wikia Emotes==
<!-- NOTE: These emotes are used as the default for ALL wikis that haven't overridden with their own MediaWiki:Emoticons page -->
*https://vignette.wikia.nocookie.net/messaging/images/7/79/Emoticon_angry.png
** (angry)
** >:-O
*https://vignette.wikia.nocookie.net/messaging/images/a/a3/Emoticon_argh.png
** (argh)
*https://vignette.wikia.nocookie.net/messaging/images/7/76/Emoticon_batman.png
** (batman)
*https://vignette.wikia.nocookie.net/messaging/images/c/cd/Emoticon_confused.png
** (confused)
** :-S
*https://vignette.wikia.nocookie.net/messaging/images/a/a2/Emoticon_cool.png
** (cool)
** B-)
*https://vignette.wikia.nocookie.net/messaging/images/1/16/Emoticon_crying.png
** (crying)
** ;-(
** :\'(
*https://vignette.wikia.nocookie.net/images/0/07/Emoticon_frustrated.png
** (frustrated)
** >:-/
*https://vignette.wikia.nocookie.net/images/7/7b/Emoticon_hmm.png
** (hmm)
*https://vignette.wikia.nocookie.net/images/b/b5/Emoticon_indifferent.png
** (indifferent)
** :-/
*https://vignette.wikia.nocookie.net/messaging/images/a/ac/Emoticon_laughing.png
** (laughing)
** :-D
*https://vignette.wikia.nocookie.net/messaging/images/1/1d/Emoticon_ninja.png
** (ninja)
*https://vignette.wikia.nocookie.net/messaging/images/5/52/Emoticon_peace.png
** (peace)
*https://vignette.wikia.nocookie.net/messaging/images/7/74/Emoticon_pirate.png
** (pirate)
** (arr)
*https://vignette.wikia.nocookie.net/messaging/images/8/8a/Emoticon_sad.png
** (sad)
** :-(
*https://vignette.wikia.nocookie.net/messaging/images/c/c2/Emoticon_silly.png
** (silly)
** :-P
*https://vignette.wikia.nocookie.net/messaging/images/8/87/Emoticon_wink.png
** (wink)
** ;-)
*https://vignette.wikia.nocookie.net/messaging/images/e/ec/Emoticon_BA.png
** (ba)
** (mrt)
*https://vignette.wikia.nocookie.net/messaging/images/7/7d/Emoticon_fingers_crossed.png
** (fingers crossed)
** (yn)
*https://vignette.wikia.nocookie.net/messaging/images/7/78/Emoticon_ghost.png
** (ghost)
** (swayze)
*https://vignette.wikia.nocookie.net/messaging/images/1/1e/Emoticon_heart.png
** (heart)
** (h)
*https://vignette.wikia.nocookie.net/central/images/8/8a/Emoticon_heidy.gif
** (heidy)
** (squirrel)
*https://vignette.wikia.nocookie.net/messaging/images/a/a9/Emoticon_stop.png
** (stop)
*https://vignette.wikia.nocookie.net/messaging/images/c/c1/Emoticon_mario.png
** (mario)
*https://vignette.wikia.nocookie.net/messaging/images/9/92/Emoticon_nintendo.png
** (nintendo)
*https://vignette.wikia.nocookie.net/messaging/images/4/40/Emoticon_no.png
** (no)
** (n)
*https://vignette.wikia.nocookie.net/messaging/images/2/2d/Emoticon_owl.png
** (owl)
*https://vignette.wikia.nocookie.net/messaging/images/c/c2/Emoticon_pacmen.png
** (pacmen)
** (pacman)
** (redghost)


==Secret Emotes==
<!-- Secret Emotes -->
```