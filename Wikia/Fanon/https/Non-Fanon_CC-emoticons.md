## Emoticons that are not hosted on Fanon, [CC](https://c.wikia.com) or the [Messaging wiki](https://messaging.wikia.com/wiki/Main_Page).
These should be replaced with emoticons that are hosted on Fanon or CC.   
Either download the image off the wiki or page they are hosted on and upload them to Fanon or find a similar image on the internet and download/upload that one to our wiki.

* http://img3.wikia.nocookie.net/__cb20131208055214/whatever-you-want/images/5/57/Tumblr_lew8wiJ4l21qek1e3o1_400.gif
 * (eevee)
 * (cutie)
 * Downloadable image: https://gitlab.com/Wychmire/Examples/blob/master/Wikia/Fanon/https/(eevee).gif
* http://images.wikia.com/legomessageboards/images/d/d5/Creeper_face.png
 * (creeper)
 *  Downloadable image: https://gitlab.com/Wychmire/Examples/blob/master/Wikia/Fanon/https/(creeper).png
* http://vignette1.wikia.nocookie.net/mspaintadventures/images/d/d6/Fruits.gif
 * (fruitparty)
 * Downloadable image: https://gitlab.com/Wychmire/Examples/blob/master/Wikia/Fanon/https/(fruitparty).gif
* http://images.wikia.com/bakugan/images/e/e1/Emoticon-Sun.gif
 * (sun)
 * Downloadable image: https://gitlab.com/Wychmire/Examples/blob/master/Wikia/Fanon/https/(sun).gif











