# Full HTTPS support for Fanon
Very soon Wikia will be using [full https on all wikis and for all users](https://community.wikia.com/wiki/User_blog:TimmyQuivy/Opt-In_Now_Available_for_HTTPS_on_FANDOM). This is a general outline/guide of what needs to be done for our wiki to use https completely. While I do want this to happen, note that we aren't in danger or anything:
> "Wikis being served in HTTP are not at an extra security risk as all sensitive functions on a wiki - such as logging in with your username and password - are already loaded through HTTPS."  
> ~ [TimmyQuivy](https://community.wikia.com/wiki/User:TimmyQuivy)

See [here](https://community.wikia.com/wiki/Thread:1440631), [here](https://community.wikia.com/wiki/Thread:1451778), and [here](https://community.wikia.com/wiki/Thread:1463616) for technical release notes from FANDOM staff.

**Note:** As of [june 6th](https://community.wikia.com/wiki/Thread:1463616), https is on by default for all accounts created *after* late march 2010. This adds extra reason to complete the following steps, as even more users will be seeing the lock-with-a-warning-sign icon by their 

Currently, HTTPS exists as an opt-in user-level preference in the [under the hood](https://community.wikia.com/wiki/Special:Preferences#mw-prefsection-under-the-hood) tab, although it will shortly be an opt-in on a wiki-level basis for those communities 

In addition to making our wiki more secure, this should in theory speed up Chat's loading time by reducing the amount of requests to different domains.

----

![Fanon's main page](https://gitlab.com/Wychmire/Examples/raw/master/Wikia/Fanon/https/Fanon-Secure.png)  
All pages *except* the emoticon page and the chat are encrypted (when the pref is checked) and the url bar looks something like the above.


![Fanon Chat, the wiki is encrypted, but the emoticons are not.](https://gitlab.com/Wychmire/Examples/raw/master/Wikia/Fanon/https/Chat-EmoticonsNotSecure.png)  
On Fanon Chat. The wiki is encrypted, but the emoticons are not, thus leading to the "Parts of this page are not secure."

----

## Chat Emoticons
Out of our 154 emoticons, only five of those use https. And two of those target certain revisions (like `/revision/latest?cb=20170824150857`)

### Stage one
**Make all emoticons hosted on Fanon or Community Central.**  
Any emoticon that isn't hosted on Fanon, Community Central, or the Messaging wiki should be downloaded and then uploaded to Fanon so we won't have any trouble with external wikis/sites changing their images or not supporting https. [here](https://gitlab.com/Wychmire/Examples/blob/master/Wikia/Fanon/https/Non-Fanon_CC-emoticons.md) for a list of emoticons that are not hosted on Fanon or CC.
 

### Stage two
**Switch all emoticon links to their https/vignette variants**  
Now for the long and rather tedious step of converting emoticons to their proper https/vignette forms. ***NO*** changes to images for this stage, only some time and patience.

All links that start with the `img#`, `images#`, or `vignette#` subdomains should be converted to their `vignette` subdomain.  
After that is done all links should be switched to https by adding an `s` to the end of `http`.

A completed version of Step two is available [here](https://gitlab.com/Wychmire/Examples/blob/master/Wikia/Fanon/https/New%20Emoticons.md), but note that it doesn't include any of stage one's changes.

----

## Imports


### Fonts
Everything is alright here as the only font ('Adelon Book') is already using https/vignette.

### JS
Not necessary as part of the https effort, but it should be done anyway.

Interwiki links should be converted to import links. `w:c:dev:` and `w:dev:mediawiki:` should be `u:dev:`

Current version:
```
// Imports
importArticles({
  type: "script",
  articles: [
    "w:c:dev:Countdown/code.js",
	"w:c:dev:UserTags/code.js",
	"w:c:dev:AjaxRC/code.js",
	"w:dev:MediaWiki:WallGreetingButton/code.js"]
});
```

Proper version: (The order is optional; I did it by size since I like the way it looks.)
```
// Imports
importArticles({
  type: "script",
  articles: [
	"u:dev:AjaxRC/code.js",
    "u:dev:UserTags/code.js",
    "u:dev:Countdown/code.js",
	"u:dev:WallGreetingButton/code.js"]
});
```