### **Verbena-grid Infobox**
Old version of the verbena-grid infobox archived here for posterities sake.

```html
<div style="display: grid; grid-auto-columns: 48.3px; grid-auto-rows: 50px; grid-gap: 6px; margin: 0 auto; width: 700px; height: 330px; background: #eee; border: 1px solid #000; padding: 3px 0 3px;">
    <!-- Left section -->
<div style="grid-column: 5; grid-row: 1; background: #e3e3e3; border: 1px solid #000;">
<div style="background: #fff; border-radius: 20px; height: 40px; width: 40px; margin: 4px auto;">
[[File:SigilSolidBlank.svg|40px|center|link=]]
<!-- Choose a sigil based on tribe. Hybrids have three options: use SigilSolidBlank.svg, their owner should create a custom sigil, or contact Forge the Hybrid with specific instructions. Available sigils for use are as follows:
[[File:SigilSolidBlank.svg|40px|center|link=]], [[File:SigilSolidHive.png|40px|center|link=]], [[File:SigilSolidIce.png|40px|center|link=]], [[File:SigilSolidLeaf.png|40px|center|link=]], [[File:SigilSolidMud.png|40px|center|link=]], [[File:SigilSolidNight.png|40px|center|link=]], [[File:SigilSolidRain.png|40px|center|link=]], [[File:SigilSolidSand.png|40px|center|link=]], [[File:SigilSolidSea.png|40px|center|link=]], [[File:SigilSolidSilk.png|40px|center|link=]], [[File:SigilSolidSky.png|40px|center|link=]]
You may delete this comment after the sigil is chosen. To view all sigils at once, head to https://wingsoffirefanon.wikia.com/wiki/User:Forge_the_Hybrid/Help:Tribe_Sigils
-->
</div>
</div>
<div style="grid-column: 1 / 6; grid-row: 2; overflow-y: auto; margin-left: 3px; background: #e3e3e3; border: 1px solid #000;">
<p style="margin: 0 4px; font-weight: bold; font-style: italic;">Family</p>
<p style="margin: 0">Waiting for Heliosanctus on this</p>
</div>
<div style="grid-column: 1 / 6; grid-row: 3; overflow-y: auto; margin-left: 3px; background: #e3e3e3; border: 1px solid #000;">
<p style="margin: 0 4px; font-weight: bold; font-style: italic;">Goal</p>
<div style="margin: 0">Example Goal</div>
</div>
<div style="grid-column: 1 / 6; grid-row: 4; overflow-y: auto; margin-left: 3px; background: #e3e3e3; border: 1px solid #000;">
<p style="margin: 0 4px; font-weight: bold; font-style: italic;">Occupation</p>
<div style="margin: 0">Example Occupation</div>
</div>
<div style="grid-column: 1 / 6; grid-row: 5; overflow-y: auto; margin-left: 3px; background: #e3e3e3; border: 1px solid #000;">
<p style="margin: 0 4px; font-weight: bold; font-style: italic;">Residence</p>
<div style="margin: 0">Example Residence</div>
</div>
    <!-- Color scheme gradients -->
    <!-- These sections should be left blank so as to properly display the colors, but they may contain text or soundcloud music. -->
<!-- A simple gradient. Replace #aaa and #ddd with your own hex codes. No text should go inside the boxes-->
<div style="grid-column: 3; grid-row: 6; background: linear-gradient(135deg, #aaa, #ddd); border: 1px solid #000;"></div>
<div style="grid-column: 4; grid-row: 6; background: linear-gradient(135deg, #aaa, #ddd); border: 1px solid #000;"></div>
<div style="grid-column: 5; grid-row: 6; background: linear-gradient(135deg, #aaa, #ddd); border: 1px solid #000;"></div>
    <!-- Center section -->
    <!-- Name and Creator -->
<div style="grid-column: 6 / 9; grid-row: 1; background: #ddd; border: 1px solid #000; text-align: center; font-weight: bold;">
<p style="margin-top: 16px; font-size:25px; line-height: 4px;">Name</p> 
<p style="font-size: 12px; line-height: 12px;">by [[User:Username|Username]]</p>
</div>
    <!-- Image -->
<div style="grid-column: 6 / 9; grid-row: 2 / 6; background: #e3e3e3; border: 1px solid #000; padding: 25px 0;">[[File:Blanktransparent.png|155px]]</div>
    <!-- Credits -->
    <!-- Change to whatever you want, if you don't need credits this can section can be used as something else. -->
<div style="grid-column: 6 / 9; grid-row: 6; overflow-y: auto; background: #e3e3e3; border: 1px solid #000; font-size: 10px; line-height: 10px;">
<p style="margin-top: 10px; text-align: center; font-size: 12px; font-weight: bold; text-decoration: underline;">Credits</p>
<p><strong>Infobox Picture by:</strong><br>
[[User:Artist]]</p>
<p><strong>Infobox design by:</strong><br>
[[User:Heliosanctus]]</p>
<p><strong>Infobox code by:</strong><br>
[[User:Forge the Hybrid]]</p>
</div>
    <!-- Section on the right -->
<div style="grid-column: 9; grid-row: 1; background: #e3e3e3; border: 1px solid #000;">
<div style="margin-top: 14px; font-size: 40px; text-align: center;">☿️ <!-- Gender icon, should be ♀, ♂, ⚥, ⚧, or ☿ --></div>
</div>
<div style="grid-column: 9 / 14; grid-row: 2 / 4; margin-right: 3px; background: #e3e3e3; border: 1px solid #000;">
<div style="border-bottom: 1px dashed #000; height: 52px; overflow-y: auto;"> 
<p style="margin: 0 4px; font-weight: bold; font-style: italic;">Allies</p>
<p style="margin: 0 4px">Name, Name, etc.</p>
</div>
<div style="height: 53px; overflow-y: auto;"> 
<p style="margin: 0 4px; font-weight: bold; font-style: italic;">Enemies</p>
<p style="margin: 0 4px">Name, Name, etc.</p>
</div>
</div>
<div style="grid-column: 9 / 14; grid-row: 4 / 6; margin-right: 3px; background: #e3e3e3; border: 1px solid #000;">
<div style="border-bottom: 1px dashed #000; height: 52px; overflow-y: auto;"> 
<p style="margin: 0 4px; font-weight: bold; font-style: italic;">Likes</p>
<p style="margin: 0 4px">Thing, Thing, etc.</p>
</div>
<div style="height: 53px; overflow-y: auto;"> 
<p style="margin: 0 4px; font-weight: bold; font-style: italic;">Dislikes</p>
<p style="margin: 0 4px">Thing, Thing, etc.</p>
</div>
</div>
<div style="grid-column: 9; grid-row: 6; width: 63px; background: #e3e3e3; border: 1px solid #000; font-weight: bold; text-align: center;">
<p style="font-size:20px; line-height:48px; margin:0;">MBTI</p>
</div>
<div style="grid-column: 10; grid-row: 6; margin-left: 16px; width: 83px; background: #e3e3e3; border: 1px solid #000; font-weight: bold; text-align: center;"><p style="margin:0;">AGE</p> 
<p style="margin: -4px 0 0;">##</p>
</div></div>
```